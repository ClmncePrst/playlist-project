import java.util.ArrayList;

public class Playlist {

    private Music currentMusic;
    private ArrayList<Music> musicList = new ArrayList<>();

    public void add(Music music) {
        musicList.add(music);
    }

    public void remove(Music music) {
        musicList.remove(music);
    }

    public void showPlaylist() {
        for (var music : musicList) {
            System.out.println(music.getInfos());
        }
        System.out.println(this.getTotalDuration());
    }

    public String getTotalDuration() {
        int total = 0;
        for (var music : musicList) {
            total += music.getDuration();
        }
        return Helper.formatDuration(total);
    }

    public void next() {
        if (this.musicList.size() > 0) {
            var first = this.musicList.get(0);
            this.currentMusic = first;
            System.out.println(this.currentMusic.getTitle());
        } else {
            System.out.println("La playlist ne contient aucune musique");
        }
    }
}
