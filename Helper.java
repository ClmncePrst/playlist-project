public class Helper {
    public static String formatDuration(int duration) {
        int minutes = duration / 60;
        int seconds = duration % 60;
        return minutes + ":" + seconds;
    }
}
