import java.util.HashSet;

class Player {
    public static void main(String[] args) {
        Artist Lens = new Artist("Amelie", "Lens");
        Artist DeWitte = new Artist("Charlotte", "DeWitte");
        System.out.println(Lens.getFullName());

        HashSet<Artist> artistSet = new HashSet<>();
        artistSet.add(Lens);
        artistSet.add(DeWitte);

        Music Drift = new Music("Drift", 400, artistSet);
        Music Radiance = new Music("Radiance", 338, artistSet);

        Playlist myPlaylist = new Playlist();
        myPlaylist.add(Drift);
        myPlaylist.add(Radiance);

        myPlaylist.showPlaylist();
        myPlaylist.next();
    }
}
